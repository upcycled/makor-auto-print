// ==UserScript==
// @name         Makor Printing Fix
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Tamper Monkey script to create page breaks for printing and fix issues when using Makor not in IE6 with ActiveX
// @author       Braydan Willrath
// @updateURL    https://gitlab.com/upcycled/makor-auto-print/-/raw/master/makor-print-fix.js
// @downloadURL  https://gitlab.com/upcycled/makor-auto-print/-/raw/master/makor-print-fix.js
// @match        *://upcycled-prod.makor-erp.com/AssetPrint.aspx*
// @match        *://upcycled-prod.makor-erp.com/ActiveXPrintViewer.aspx*
// @match        *://upcycled-prod.makor-erp.com/ActiveXPrintViewerMulti.aspx*
// @icon         https://www.google.com/s2/favicons?domain=makor-erp.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    window.close = function() {
        console.log("Prevented JS code call for webpage close.");
    }
    // Your code here...

    function load() {
        var injecting_style = `
  tr {page-break-after: always;page-break-inside: avoid;} span#lblSkidDetails, span#lblSkidDetails span {font-size: 16px!important;}
  `;
    var head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');

head.appendChild(style);

style.type = 'text/css';
if (style.styleSheet){
  // This is required for IE8 and below.
  style.styleSheet.cssText = injecting_style;
} else {
  style.appendChild(document.createTextNode(injecting_style));
}
        resize();
         window.print();
    }

    setTimeout(load, 2);

    function resize() {
        var innerWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var innerHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        var targetWidth = 1024;
        var targetHeight = 768;
        window.resizeBy(targetWidth-innerWidth, targetHeight-innerHeight);
    }
})();
